# Docker Virtualization.framework performance boost

If you are working with a Mac and are either running an amd64 (Intel processor) or arm64 (M1 processor) machine, you'll have likely come across an issue with performance compared to working directly with Docker on Linux.

Fundamentally, the issue stems from Volumes being synchronixed from the host OS into the Docker virtual drive where as Linux conversely mounts its filesystems directly into the containers. As such, Docker for Mac has to leverage an extra step to get files working properly and thus performace is lost.

Over the years, the Docker development team has been working hard to improve this performance issue and the solutions have gotten us closer to parity with Linux. But we are still a good ~200% loss in efficiency in some cases. Turn on XDEBUG in your development environment and you can basically bring docker down to a crawl.

So, what can we do to resolve this in the interim?

Looking to several possible solutions, the one which seems to give the best results drew from this original article: [https://piyush-agrawal.medium.com/setting-up-linux-vm-on-apple-silicon-for-docker-e5b9924fd09](https://piyush-agrawal.medium.com/setting-up-linux-vm-on-apple-silicon-for-docker-e5b9924fd09). I later found a subsequent article which expands on the idea and makes it useful for M1 computers as well: [https://medium.com/carvago-development/my-docker-on-macos-part-1-setup-ubuntu-virtual-machine-both-intel-and-apple-silicon-cpu-5d886af0ebba](https://medium.com/carvago-development/my-docker-on-macos-part-1-setup-ubuntu-virtual-machine-both-intel-and-apple-silicon-cpu-5d886af0ebba).

The goal of this README is to leverage these two solutions, build on it and help to provide developers with a set of steps and scripts to get you to this final piece quickly.

## Diggin' In!

First of all, we need to place all our hard work somewhere:

```shell
mkdir ~/ubuntu-vm
cd ~/ubuntu-vm
```

Once we have our folder, need to build all the necessary files. All the steps our outlined below

## Download all files needed for Ubuntu 20.04 VM — Choose depending on your M1 or Intel based CPU

**M1 (ARM64):**

```shell
curl -o initrd https://cloud-images.ubuntu.com/focal/current/unpacked/focal-server-cloudimg-arm64-initrd-generic

curl -o kernel.gz https://cloud-images.ubuntu.com/focal/current/unpacked/focal-server-cloudimg-arm64-vmlinuz-generic

gunzip kernel.gz

curl -o disk.tar.gz https://cloud-images.ubuntu.com/releases/focal/release/ubuntu-20.04-server-cloudimg-arm64.tar.gz
```

**Intel based (AMD64):**

```shell
curl -o initrd https://cloud-images.ubuntu.com/focal/current/unpacked/focal-server-cloudimg-amd64-initrd-generic

curl -o kernel https://cloud-images.ubuntu.com/focal/current/unpacked/focal-server-cloudimg-amd64-vmlinuz-generic

curl -o disk.tar.gz https://cloud-images.ubuntu.com/releases/focal/release/ubuntu-20.04-server-cloudimg-amd64.tar.gz
```

## Unzip files + clean up

```shell
tar xvfz disk.tar.gz
rm README disk.tar.gz
mv focal-server-* disk.img
```

## Download and build [vftool](https://github.com/evansm7/vftool)

```shell
git clone git@github.com:evansm7/vftool.git
cd vftool
make
cd ..

```

## Run VM for the first time

When you run the VM instructions, you can change the memory value to whatever you require. It is presently set to 4G (4096), but if you have a system with more memory, you can set this to 8G (8192) for more room.

```shell
vftool/build/vftool -k kernel -i initrd -d disk.img -m 8192 -a "console=hvc0"
```

Once you've run the above command you'll see something that looks a bit like this:

```bash
2021-08-18 13:34:42.838 vftool[45393:20372365] vftool (v0.3 10/12/2020) starting
2021-08-18 13:34:42.838 vftool[45393:20372365] +++ kernel at kernel, initrd at initrd, cmdline 'console=hvc0', 1 cpus, 8192MB memory
2021-08-18 13:34:42.842 vftool[45393:20372365] +++ fd 3 connected to /dev/ttys003
2021-08-18 13:34:42.842 vftool[45393:20372365] +++ Waiting for connection to:  /dev/ttys003
```

Open new terminal window and run this command to connect to VM for the first time (please note, there will be random number and it might not always be 003 in `/dev/ttys003`:

```bash
screen /dev/ttys003
```

You should see a bunch of Linux bootstrapping output and then a prompt that looks something like this:

```bash
(initramfs)
```

Keep this window alive as we'll be coming back to it several times.

## Sourcing the VM IP Address

As it will become obvious, knowing the VM's IP address is paramount to having us work with our Ubuntu solution. Use the following command in a separate terminal window to get the IP address:

```bash
ifconfig bridge100 | grep 'inet ' | awk '{print $2}'
```

This command will give you something that looks like this: `192.168.64.1` - the key here is changing the last digit on the IP address. You can use whatever you want between `2` ~ `255`. For the sake of this example, we're going to use `2`.

## Building up the VM's keys and network

The following commands will be executed on the `screen /dev/ttys003` terminal at the `initramfs` prompt:

```bash
mkdir /mnt
mount /dev/vda /mnt
chroot /mnt

touch /etc/cloud/cloud-init.disabled

echo 'root:root' | chpasswd

ssh-keygen -f /etc/ssh/ssh_host_rsa_key -N '' -t rsa
ssh-keygen -f /etc/ssh/ssh_host_dsa_key -N '' -t dsa
ssh-keygen -f /etc/ssh/ssh_host_ed25519_key -N '' -t ed25519
```

Next we'll create the `netplan` file Ubuntu will use to get the network resolving with the IP we determined above:

```bash
cat <<EOF > /etc/netplan/01-dhcp.yaml 
network:
    version: 2
    ethernets:
        enp0s1:
            dhcp4: true
            addresses: [192.168.64.2/24]
            nameservers:
                addresses: [8.8.8.8, 8.8.4.4]  
EOF
```

Once this is done, we can `exit` the `screen` and kill the VM with `CTRL+C` in the original terminal window we ran the `vftool`.

This will be one of the few times we do a `CTRL-C` on the VM as it can be very detrimental on the image to kill the VM without warning. In the future, we will use the `halt` command from the prompt.

## Resize disk to 100GB

*Note that space will be fully allocated and blocked, even if you use only some of it.*

Original disk is very small, let's resize it to 100GB (change it to any other desired value).

**This operation might take several minutes to finish**:

```bash
dd if=/dev/zero bs=1m count=100000 >> ./disk.img
```

If you encounter `dd: invalid number: '1m'` change `1m` to `1M`.

Now start VM again (*remember to match the memory value used previously*):

```bash
vftool/build/vftool -k kernel -i initrd -d disk.img -m 8192 -a "root=/dev/vda console=hvc0"
```

Connect to the VM on a new terminal window with `screen`. Login using **root** (password: **root**). Let’s confirm if the resize worked. Run `df -h | grep vda` to check the size of `/dev/vda`. If it is `1.3G` instead of what you resized too, let’s verify through `parted` if the `dd` command worked.

```bash
$ parted
(parted) print devices
/dev/vda (106GB)
(parted) quit
```

If the output above shows the correct size that you expanded to, run `resize2fs /dev/vda` to resize the partition to max. Running `df -h` should now show the correct size.

Keep the VM running.

## Set up ssh key

Run `pbcopy < ~/.ssh/id_rsa.pub` in the host's terminal. This will copy the key to the clipboard.

Now change into VM's terminal and run `mkdir ~/.ssh` to create directory, `cat > ~/.ssh/authorized_keys`, paste the output on the new line displayed and hit `CTRL-D`. This will kill the stdin and save the paste to your file.

Then run `chmod 700 ~/.ssh && chmod 600 ~/.ssh/authorized_keys`

Your VM is all set up now and you should never have to use the `screen` command again to enter it!

Let's stop and shutdowm the VM:

```bash
halt
```

If all has gone well, you might see a series of instructions stopping services and finally...

```bash
...
...
...
[  OK  ] Reached target Shutdown.
[  OK  ] Reached target Final Step.
         Starting Halt...
[ 7587.214400] reboot: System halted
```

We can safely `CTRL-C` the VM terminal as well.

## Wrapping it all up!

### RIP localhost

It might be obvious, but *better safe than sorry. Working with Docker will prove a bit challenging now as certain approaches to navigating services and accessing dev websites will require some manual consideration. Since the domain `localhost` requires the use of `127.0.0.1` and the VM is on a totally different network, addresses like `http://localhost:8080` won't work.To resolve this, we can access the VM directly through `192.168.64.2` -- this is the address we collected and provided to the VM. Since `http://192.168.64.2:8080` isn't the shiniest of solutions, we will give our VM a hostname: `http://ubuntu:8080`. Our example here can be setup to use almost anything you want. This is a discretionary value.

```bash
echo -e '192.168.64.2 ubuntu' | sudo tee -a /etc/hosts
```

### Global command alias for starting the VM

This will create command `start-ubuntu` that will start your VM. If you are using `bash`, you will need to replace `.zshrc` with `.bashrc`. The below example uses `zsh`. You should also tweak the memory value (`8192`) to match the one you used in previous steps.

```bash
echo 'alias start-ubuntu="~/ubuntu-vm/vftool/build/vftool -k ~/ubuntu-vm/kernel -i ~/ubuntu-vm/initrd -d ~/ubuntu-vm/disk.img -m 8192 -a \"root=/dev/vda console=hvc0\" -t 0"' >> ~/.zshrc
```

Note the command contains `-t 0` which permits the console to use stdin/stdout. Review the vftool documentation for more information.

### Install Docker on VM

Follow official documentation: [https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository](https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository)

Verify that you can run in VM `docker run hello-world`

Do not forget to install `docker-compose`: [https://docs.docker.com/compose/install/](https://docs.docker.com/compose/install/)

### Add non-root user (optional)

I created `janmikes` user for Docker (change to your name, obviously 😊). I recommend having same user name as you do on your mac.

```bash
adduser newuser
usermod -aG sudo newuser
usermod -aG docker newuser
```

Disable need of password when running commands with `sudo` as user, run `visudo` and add this to the end of file:

```bash
newuser ALL=(ALL) NOPASSWD: ALL
```

Copy ssh key that we previously added so you can have passwordless ssh to non-root user as well:

```bash
su newuser
mkdir ~/.ssh
sudo cat /root/.ssh/authorized_keys > ~/.ssh/authorized_keys
chmod 700 ~/.ssh
chmod 600 ~/.ssh/authorized_keys
```

To verify that you can SSH to VM as regular user and run Docker commands without sudo run this from your mac terminal:

```bash
ssh ubuntu 'docker run hello-world'
```

### NFS Volumes and MacOS Mount points

If you are interested in working with NFS mount points with your ubuntu VM, we'll have to do some custom installations and expose some functionality. First step is to install the NFS server and a few other tools to help us get all the way to the end goal:

```bash
sudo apt-get install nfs-kernel-server
sudo apt-get install make
```

Once these are in place, we need to setup both the Linux server to serve the share point and get the Mac OS to mount it. This allows us to use the Mac host with its IDE's to work on the files in the VM.

#### Setting up the Linux Server

1. Edit `/etc/exports`:
   ```bash
   sudo nano /etc/exports
   ```
2. Add a line in the file similar to this:
   ```bash
   /home/ubuntu 192.168.64.0/24(insecure,rw,all_squash,anonuid=1000,anongid=1000,no_subtree_check)
   ```

   * `/srv/nsf4` is the directory to export
   * `192.168.64.0/24` is the IP addresses to accept connections from. The Mac client's IP address should be in this range. Use `*` to allow from any IP address. (But be careful not to make your NFS server available to the entire internet!)
   * `insecure` means to accept connections from unprivileged (higher) port numbers
   * `rw` means read-write
   * `all_squash,anonuid=1000,anongid=1000` forces all reads and writes to be performed by the user/group with UID/GID 1000 (1000 is the default `ubuntu` user/group on my server). Run `id` on the server to find out your UID/GID. You need these options unless your Ubuntu server and Mac client use the same UID/GID for the main user.
   * `no_subtree_check` is a performance option
3. Save the file and run
   ```bash
   sudo exportfs -vra
   ```

   to reload the NFS exports. (I'm not sure if the -a option is necessary.)

#### Setting up the Mac client

1. On the macOS client, edit the `/etc/auto_master` file (documented in the [auto_master man page](https://www.unix.com/man-page/osx/5/auto_master/#neo-man-table-output)):
   ```bash
   sudo nano /etc/auto_master
   ```
   and change the line starting with `/net` to the following (or add it if necessary):
   ```bash
   /net -hosts -nobrowse,nosuid,locallocks,nfc,actimeo=1
   ```
   * `locallocks` creates locks on the client rather than on the server. Without this, Finder becomes very slow and takes forever to show directories.
   * `nfc` makes UTF-8 file names work
   * `actimeo=1` sets the attribute cache timeout as short as possible. Note that setting it to `0` (or adding `noac`) causes Finder not to notice when a file is deleted on the server, so we can't use it.
   * Note that we're not using `nfsvers=4` here. I got kernel panics on the Mac with this, so I went back to the default (NFSv3).

   Note: It appears that some macOS software updates can overwrite this file and remove your changes. I've found myself having to go back to back to this answer once a year or so re-apply the changes.
   
2. Refresh the automounts by running
   ```bash
   sudo automount -vc
   ```
   (If you previously tried to mount an NFS volume, unmount it first, like so: `sudo umount -f /net/ubuntu/srv/nfs4`)
   
3. In the Finder menu, select Go -> Go to Folder, and type `/net/SERVER_HOST_NAME`, e.g. `/net/ubuntu`.
   You should find your exported directory in there, e.g. at `/net/ubuntu/srv/nfs4`. Drag this directory to the Finder sidebar to make it easy to access in the future.
   
### BACKUP!!! (optional)

Do not forget to backup your disk! Now, when you have Docker, non-root user, ZSH, it is the best time to create a backup!

**IMPORTANT: Please shutdown the VM before running the backup.**

Using `tar` command on 100GB disk took on my M1 mac like 5 minutes to finish with result of 1.15GB backup file:

```bash
tar -zcvf disk-backup.img.tar.gz disk.img
```

### Using Docker for Mac (optional)

Once last thing, since we will now be doing VM Docker, we might as well allow ourselves a way to leverage it without having to `ssh` into the container everytime.

To get `docker` to run from the host cli, change the `DOCKER_HOST` variable (add to shell rc file) so all Docker commands run inside the VM:

```bash
export DOCKER_HOST=ssh://newuser@ubuntu
```

This is a handy trick to provide a shortcut, though it has been shown to be a bit more time consuming this way. Use at your discretion.

### Some Extras to brighten up your day!

If you have not done so, setting up your environment to us ZSH as a shell can really help to improve your experience. Further to that are some really cool steps you can take to leverage ZSH and its many plugins to add some superior shell shortcuts.

- [https://ohmyz.sh/](https://ohmyz.sh) -- a very powerful wrapper for zsh.
  - [https://gist.github.com/kevin-smets/8568070](https://gist.github.com/kevin-smets/8568070) -- this is a very useful guide around what to install and how to set things up using oh-my-zsh
  - It is strongly recommended to install the **Powerlevel9k / Powerlevel10k** plugin and theme. This will significantly improve your prompt-fu!
- When installing docker-compose on the Linux side of things (even on the mac for that matter) consider looking into the tab completion capability: [https://docs.docker.com/compose/completion/](https://docs.docker.com/compose/completion/)

## References

I wish to thank a bunch of people for all their hard work to get us here:

- [https://accesto.com/blog/docker-on-mac-how-to-speed-it-up/](https://accesto.com/blog/docker-on-mac-how-to-speed-it-up/)
- [https://serverfault.com/questions/716350/mount-nfs-volume-on-ubuntu-linux-server-from-macos-client](https://serverfault.com/questions/716350/mount-nfs-volume-on-ubuntu-linux-server-from-macos-client)
- [https://piyush-agrawal.medium.com/setting-up-linux-vm-on-apple-silicon-for-docker-e5b9924fd09](https://piyush-agrawal.medium.com/setting-up-linux-vm-on-apple-silicon-for-docker-e5b9924fd09)
- [https://medium.com/carvago-development/my-docker-on-macos-part-1-setup-ubuntu-virtual-machine-both-intel-and-apple-silicon-cpu-5d886af0ebba](https://medium.com/carvago-development/my-docker-on-macos-part-1-setup-ubuntu-virtual-machine-both-intel-and-apple-silicon-cpu-5d886af0ebba)
